package sample;

public class UserNotFoundException extends RuntimeException {

    UserNotFoundException(String message) {

        super(message);
    }
}
