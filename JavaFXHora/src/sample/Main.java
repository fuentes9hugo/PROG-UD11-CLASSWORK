package sample;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class Main extends Application {

    @Override // Override the start method in the Application class
    public void start(Stage primaryStage) {

        Scene scene = new Scene(pane(), 500, 400);
        primaryStage.setTitle("Time");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

    private GridPane pane() {

        GridPane pane = new GridPane();

        Button btRefresh = new Button("refresh");
        btRefresh.setId("btRefresh");

        LocalDateTime madridTime = LocalDateTime.now(ZoneId.of("Europe/Madrid"));
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("hh:mm:ss");

        Label madridLabel = new Label(madridTime.format(formatter));

        Text madridText = new Text("Madrid");

        LocalDateTime londonTime = LocalDateTime.now(ZoneId.of("Europe/London"));

        Label londonLabel = new Label(londonTime.format(formatter));

        Text londonLext = new Text("London");

        madridText.setFont(Font.font(20));
        londonLext.setFont(Font.font(20));

        madridLabel.setFont(Font.font(20));
        londonLabel.setFont(Font.font(20));

        pane.add(madridText,0,0);
        pane.add(londonLext,2,0);
        pane.add(madridLabel,0,1);
        pane.add(londonLabel,2,1);
        pane.add(btRefresh,1,2);

        pane.setAlignment(Pos.CENTER);

        btRefresh.setOnAction(event -> change(madridLabel,londonLabel));

        return pane;
    }

    private void change(Label madridLabel, Label londonLabel) {

        LocalDateTime madridTime = LocalDateTime.now(ZoneId.of("Europe/Madrid"));
        LocalDateTime londonTime = LocalDateTime.now(ZoneId.of("Europe/London"));
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("hh:mm:ss");

        madridLabel.setText(madridTime.format(formatter));

        londonLabel.setText(londonTime.format(formatter));
    }
}
