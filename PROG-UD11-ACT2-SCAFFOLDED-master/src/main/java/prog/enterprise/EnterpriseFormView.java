package prog.enterprise;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.util.Duration;
import prog.TextFieldErrorLabel;
import prog.Validator.Validator;

import java.util.Locale;

public class EnterpriseFormView extends GridPane {

    private Label idLabel;
    private TextField idTextField;

    private Label enterpriseNameLabel;
    private TextFieldErrorLabel enterpriseNameTextField;

    private Label nifLabel;
    private TextFieldErrorLabel nifTextField;

    private Label addressLabel;
    private TextFieldErrorLabel addressTextField;

    private Label cityLabel;
    private TextFieldErrorLabel cityTextField;

    private Label provinceLabel;
    private TextFieldErrorLabel provinceTextField;

    private Label countryLabel;
    private ChoiceBox<String> countryChoiceBox;

    private Label localeLabel;
    private ChoiceBox<Locale> localeChoiceBox;

    private Label statusLabel;
    private CheckBox statusCheckbox;

    private EventHandler<MouseEvent> formEventHandler = new FormEnterpriseEventHandler();

    private Label infoLabel;
    private VBox infoPanel;

    private Validator validator;
    private EnterpriseRepository enterpriseRepository;

    public EnterpriseFormView(Validator validator, EnterpriseRepository enterpriseRepository){

        initView();
        initInfoPanel();
        this.validator = validator;
        this.enterpriseRepository = enterpriseRepository;

    }

    private void initView(){

        this.setAlignment(Pos.CENTER);
        this.setHgap(10);
        this.setVgap(10);
        this.setPadding(new Insets(15, 15, 30, 15));

        Text formTitle = new Text("Enterprise Form");
        formTitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        this.add(formTitle, 0, 0, 2, 1);
        GridPane.setHalignment(formTitle, HPos.CENTER);

        /** Tipo textfield**/
        this.idLabel = new Label("Id: ");
        this.idTextField = new TextField();
        this.idTextField.setEditable(false);
        this.enterpriseNameLabel = new Label("Name:");
        this.enterpriseNameTextField = new TextFieldErrorLabel();
        this.nifLabel = new Label("Nif:");
        this.nifTextField = new TextFieldErrorLabel();
        this.addressLabel = new Label("Address: ");
        this.addressTextField = new TextFieldErrorLabel();
        this.cityLabel = new Label("City: ");
        this.cityTextField = new TextFieldErrorLabel();
        this.provinceLabel = new Label("Province: ");
        this.provinceTextField = new TextFieldErrorLabel();
        this.add(idLabel,0,1);
        this.add(idTextField,1,1);
        this.add(enterpriseNameLabel,0,2);
        this.add(enterpriseNameTextField,1,2);
        this.add(nifLabel,0,3);
        this.add(nifTextField,1,3);
        this.add(addressLabel,0,4);
        this.add(addressTextField,1,4);
        this.add(cityLabel,0,5);
        this.add(cityTextField,1,5);
        this.add(provinceLabel,0,6);
        this.add(provinceTextField,1,6);

        /** Tipo choicebox**/
        this.countryLabel = new Label("Country:");
        this.countryChoiceBox = new ChoiceBox<>(getValidCountries());
        this.countryChoiceBox.setValue("ES");
        this.localeLabel = new Label("Locale: ");
        this.localeChoiceBox = new ChoiceBox<>(getValidLocales());
        this.localeChoiceBox.setValue(Locale.ENGLISH);
        this.add(countryLabel,0,7);
        this.add(countryChoiceBox,1,7);
        this.add(localeLabel,0,8);
        this.add(localeChoiceBox,1,8);

        /** Tipo checkbox**/
        this.statusLabel = new Label("Status:");
        this.statusCheckbox = new CheckBox();
        this.statusCheckbox.setSelected(true);
        this.add(statusLabel,0,9);
        this.add(statusCheckbox,1,9);

        Button saveButton = new Button("Save");
        this.add(saveButton, 0, 10,2,2);
        GridPane.setHalignment(saveButton, HPos.CENTER);
        saveButton.setId("save-bt");
        saveButton.addEventHandler(MouseEvent.MOUSE_CLICKED ,this.formEventHandler);

        /** Crear componentes y añadirlos al panel */
    }

    private void initInfoPanel(){

        infoLabel = new Label("");
        Label titleInfoLabel = new Label("info:");
        infoPanel = new VBox(titleInfoLabel,infoLabel);
        infoPanel.getStyleClass().add("info-panel");
        infoPanel.setVisible(false);
        this.add(infoPanel, 0, 0, 2, 1);
        GridPane.setHalignment(infoPanel, HPos.CENTER);

    }

    private ObservableList<Locale> getValidLocales() {

        return FXCollections.observableArrayList(Locale.getAvailableLocales());

    }

    private ObservableList<String> getValidCountries() {

        return FXCollections.observableArrayList(Locale.getISOCountries());

    }

    private void submitForm(){

        DbEnterpriseRepository dbEnterpriseRepository = new DbEnterpriseRepository();

        if (validateForm()) {
            Enterprise enterprise = getEnterprise();

            dbEnterpriseRepository.save(enterprise);

            idTextField.setText(enterprise.getId().toString());

            showInfoMessage("Insertado");
        }

    }

    private boolean validateForm() {

        DbEnterpriseRepository dbEnterpriseRepository = new DbEnterpriseRepository();

        boolean isValid = true;

        if (!this.validator.validateString(enterpriseNameTextField.getText(), 1, 45)) {
            enterpriseNameTextField.setError("* must be between 1 to 45 characters");
            isValid = false;
        } else {
            enterpriseNameTextField.clear();
        }

        if (!this.validator.validateString(addressTextField.getText(), 1, 45)) {
            addressTextField.setError("* must be between 1 to 45 characters");
            isValid = false;
        } else {
            addressTextField.clear();
        }

        if (!this.validator.validateString(cityTextField.getText(), 1, 45)) {
            cityTextField.setError("* must be between 1 to 45 characters");
            isValid = false;
        } else {
            cityTextField.clear();
        }

        if (!this.validator.validateString(provinceTextField.getText(), 1, 45)) {
            provinceTextField.setError("* must be between 1 to 45 characters");
            isValid = false;
        } else {
            provinceTextField.clear();
        }

        if (!this.validator.validateDNI(nifTextField.getText())) {
            nifTextField.setError("* wrong NIF");
            isValid = false;
        } else {
            nifTextField.clear();
        }

        if (dbEnterpriseRepository.findByNif(nifTextField.getText()) != null) {
            nifTextField.setError("* exsisting NIF");
            isValid = false;
        } else {
            nifTextField.clear();
        }

        return isValid;

    }

    private Enterprise getEnterprise() {

        return new Enterprise(
                enterpriseNameTextField.getText(),
                addressTextField.getText(),
                cityTextField.getText(),
                provinceTextField.getText(),
                countryChoiceBox.getValue(),
                localeChoiceBox.getValue().toString(),
                nifTextField.getText());

    }

    private void showInfoMessage(String message){

        this.infoPanel.setVisible(true);
        this.infoLabel.setText(message);
        Timeline fiveSecondsWonder = new Timeline(new KeyFrame(Duration.seconds(3), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                infoPanel.setVisible(false);
            }
        }));
        fiveSecondsWonder.play();

    }

    class FormEnterpriseEventHandler implements EventHandler<MouseEvent>{
        @Override
        public void handle(MouseEvent event) {

            String controlId = ((Control) event.getSource()).getId();

            if (controlId.equals("save-bt")) {

                submitForm();

            }

        }
    }
}
