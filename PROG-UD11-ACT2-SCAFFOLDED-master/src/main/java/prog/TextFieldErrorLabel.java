package prog;


import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;

public class TextFieldErrorLabel extends VBox {

    private TextField textField;
    private Label errorLabel;

    public TextFieldErrorLabel() {
        super();
        textField = new TextField();
        errorLabel = new Label();
        errorLabel.getStyleClass().add("error-msg");
        getChildren().add(textField);
        getChildren().add(errorLabel);
        clear();
    }

    public void setError(String text){
        errorLabel.setText(text);
        errorLabel.setVisible(true);
        errorLabel.setManaged(true);
    }

    public void clear(){
        errorLabel.setText("");
        errorLabel.setVisible(false);
        errorLabel.setManaged(false);
    }

    public String getText() {
        return textField.getText();
    }

}
