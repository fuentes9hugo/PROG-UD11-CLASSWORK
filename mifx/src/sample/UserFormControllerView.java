package sample;

import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

public class UserFormControllerView {

    @FXML
    private TableView<User> userTable;
    @FXML
    private TableColumn<User, String> firstNameColumn;
    @FXML
    private TableColumn<User, String> lastNameColumn;

    @FXML
    private Label idLabel;
    @FXML
    private Label nameLabel;
    @FXML
    private Label firstNameLabel;
    @FXML
    private Label phoneNumberLabel;
    @FXML
    private Label lastLoginLabel;
    @FXML
    private Label activeLabel;
    @FXML
    private Label birthdayLabel;

    private DbUserRepository dbUserRepository;

    public UserFormControllerView(DbUserRepository dbUserRepository){

        this.dbUserRepository = dbUserRepository;
    }
    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {

        userTable.setItems(getUserList());

        userTable.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> showUserDetails(newValue)
        );

        firstNameColumn.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().getFirstName()));
        lastNameColumn.setCellValueFactory((cellData) -> new ReadOnlyStringWrapper(cellData.getValue().getLastName()));

    }

    public ObservableList<User> getUserList() {

        return FXCollections.observableArrayList(this.dbUserRepository.findUsersByEnterprise(1,0));
    }

    private void showUserDetails(User user) {

        idLabel.setText(user.getId().toString());
        nameLabel.setText(user.getFirstName());
        firstNameLabel.setText(user.getLastName());
        phoneNumberLabel.setText(user.getPhoneNumber());
        lastLoginLabel.setText(user.getLastLogin());
        activeLabel.setText(user.isActive().toString());
        birthdayLabel.setText(user.getBirthday());
    }
}
