package sample;

import java.util.ArrayList;

public interface UserRepository {

    boolean saveUser(User user);

    User findById(int id);

    User getById(int id);

    User findUserByEmail(String email);

    ArrayList<User> findUsersByEnterprise(int idEnterprise, int page);

    boolean dropUser(int idUser);
}
